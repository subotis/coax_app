Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    get 'list', to: 'user#list_tracked', format: false
    post 'track', to: 'user#set_tracking_currency', format: false
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
