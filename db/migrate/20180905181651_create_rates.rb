class CreateRates < ActiveRecord::Migration[5.2]
  def change
    create_table :rates do |t|
      t.references :user, foreign_key: true
      t.string :from, limit: 5, null: false
      t.string :to, limit: 5, null: false
      t.string :amount

      t.timestamps
    end
  end
end
