# README

API key placed in credentials. Master key I will send with link to repo.

Before launch change external API key to new one, as they have limit to 50 queries per month.

Which spends very fast due to 1 minute * number_of_rates queries.

As for me - better to requery all rates at once, rather then query them based on user input.

Rates should be connected to user, so I implement basic cookie-based auth.

Of course I know that for API is better to use OAuth or JWT, but this is additional work, and params to request.

## Requests
to get current user tracking rates visit:

/api/list (Method GET)

response will be something like: 

{
    "rates": [
        {
            "from": "RUB",
            "to": "PLN",
            "amount": "0"
        }
    ]
}

to add rate to tracking make POST request to

/api/track

as JSON with content:

{
  "from": "RUB",
  "to": "PLN" 
}

Also here some kind of db cleaning option - I don't know how much users will use it.
So to remove unnecesary jobs every week app will remove users which was inactive more then week. And all associated rates to that users will be destroyed.

Anyway if this app should be like "proxy" less consuming approach as for me is to grab all rates at once every minute, rather then make same queries with different time of update.
#Stack
* ruby - 2.5.1
* rails - 5.2.1

#Deployment

* Clone repo
* Copy attached master.key into {APP_PATH}/config folder
* install gems
* rails db:migrate
* EDITOR="nano" bin/rails credentials:edit (replace nano with your favorite editor)
* edit in credentials file api: access_key: 'your_amorden_api_key'
* start rails server



