class User < ApplicationRecord
  has_many :rates, dependent: :delete_all
end
