class ClearNonActiveUsersJob
  include SuckerPunch::Job

  def perform
    ActiveRecord::Base.connection_pool.with_connection do
      users = User.where("last_action_at <= ?", Time.now - 1.week)
      users.each(&:destroy)
    end
    ClearNonActiveUsersJob.perform_in(60 * 60 * 24 * 7)
  end
end
