# frozen_string_literal: true

class RaterJob
  include SuckerPunch::Job

  def perform(rate)
    response = HTTParty.get("https://www.amdoren.com/api/currency.php?api_key=#{Rails.application.credentials.api[:access_key]}&from=#{rate.from}&to=#{rate.to}")
    ActiveRecord::Base.connection_pool.with_connection do
      if Rate.exists?(rate.id)
        rate.update_attribute(:amount, JSON.parse(response.body)['amount']) if response.success?
      end
    end
    # remove from AR blocking
    RaterJob.perform_in(60, rate) if Rate.exists?(rate.id)
  end
end
