module UserConcern
  extend ActiveSupport::Concern

  def set_user
    @user = User.where(id: cookies.encrypted[:user]).first_or_create
    @user.update_columns(last_action_at: Time.now)
    cookies.encrypted[:user] = { value: @user.id, expires: 1.year.from_now }
  end
end