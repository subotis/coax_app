# frozen_string_literal: true

class Api::UserController < ApplicationController
  include UserConcern
  before_action :set_user
  ACCEPTED_CURRENCY = %w[AED AFN ALL AMD ANG AOA ARS AUD AWG AZN BAM BBD BDT BGN BHD BIF BMD BND BOB BRL BSD BTN BWP BYN BZD CAD CDF CHF CLP CNY COP CRC CUC CVE CZK DJF DKK DOP DZD EGP ERN ETB EUR FJD GBP GEL GHS GIP GMD GNF GTQ GYD HKD HNL HRK HTG HUF IDR ILS INR IQD IRR ISK JMD JOD JPY KES KGS KHR KMF KPW KRW KWD KYD KZT LAK LBP LKR LRD LSL LYD MAD MDL MGA MKD MMK MNT MOP MRO MUR MVR MWK MXN MYR MZN NAD NGN NIO NOK NPR NZD OMR PAB PEN PGK PHP PKR PLN PYG QAR RON RSD RUB RWF SAR SBD SCR SDG SEK SGD SHP SLL SOS SRD SSP STD SYP SZL THB TJS TMT TND TOP TRY TTD TWD TZS UAH UGX USD UYU UZS VEF VND VUV WST XAF XCD XOF XPF YER ZAR ZMW].freeze

  def set_tracking_currency
    if ACCEPTED_CURRENCY.include?(currency_params[:from]) && ACCEPTED_CURRENCY.include?(currency_params[:to])
      @rate = Rate.where(user_id: @user.id, from: currency_params[:from], to: currency_params[:to]).first
      # just to ensure that we dont queue job with same params multiple times
      unless @rate
        @rate = Rate.create(user_id: @user.id, from: currency_params[:from], to: currency_params[:to])
        RaterJob.perform_in(60, @rate)
      end
      render json: @rate.to_json(only: %i[from to]), status: :ok
    else
      render json: { error: 'Currency is not allowed' }, status: :unprocessable_entity
    end
  end

  def list_tracked
    render json: { rates: @user.rates.as_json(only: %i[from to amount]) }, status: :ok
  end

  private

  def currency_params
    params.require(%i[from to])
    params.permit(%i[from to])
  end
end
